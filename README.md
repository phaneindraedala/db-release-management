* Create a aurora db instance
```
Details:

db instance identifier: dba-release-automation
Master username: dbatest
pass: Welcome1


db cluster identifier: dba-release-automation-cluster
database name: dbnametest
```
* Pull mysql client image
```
docker pull activatedgeek/mysql-client
```
* Run container
```
docker run --rm -it --entrypoint sh activatedgeek/mysql-client:latest
```
#### Connect to aurora DB from container
* After logging in to container terminal using the above step.
* Enter the below command to connect to the db
```
mysql --user=dbatest --password=Welcome1 -h dba-release-automation.chvbn1t7swpp.us-west-2.rds.amazonaws.com -D dbnametest

Syntax: mysql --user=master_username --password=master_password -h fqdn or IP -D database name
```
* You will prompted with the password prompt enter the master password
* You will see the below prompt
```
MySQL [dbnametest]>
```
* Type the below command to create a database
```
create database phani_demo;
```
* Now type the below command to view the created database
```
show databases;

O/P:
+--------------------+
| Database           |
+--------------------+
| information_schema |
| dbnametest         |
| mysql              |
| performance_schema |
| phani_demo         |
+--------------------+
```
